using UnityEngine;
using System.Collections;

///<TT>Interpolate.cs</TT> is a simple script for the main menu screen
public class Interpolate : MonoBehaviour {
	///<TT>Update()</TT> changes a block on the screen from blue to red over time and back again
	///
	void Update () {
			this.gameObject.renderer.material.color = Color.Lerp( Color.blue, Color.red, Time.time);
	
	}
}

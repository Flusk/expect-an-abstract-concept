using UnityEngine;
using System.Collections;

/*! \brief
<TT>BlockBehaviour.cs</TT> is a script that holds and controls the rule set for how the blocks in the game behave.<BR> This rule set configurations are:
<UL>
<LI>Setting the the conditions to allow blocks to render.</LI>
<LI>Setting the conditions to allow the player when the blocks should render (or not render).</LI>
<LI>Checking for collision wiith the Avatar</LI>
</UL>
*/

public class BlockBehaviour : MonoBehaviour {
	
	//public float constTimer = 3.0f;
	//public float decTimer;
	CollectPills cp; 						/*!<Member to access the <TT>CollectPills</TT> component in the Avatar (Player) */

	//public int amountOfRendered;
	public bool collided = false;			/*!<Member to allow for checking if the the block has been touched by the Avatar  */

	bool rendered;							/*!<A bool member to keep track if the block has been rendered */
	public float decayTimer = 20.0f;		/*!<The value that the timer will countdown from and reset to (if necessary) */
	public float decrementDecayTimer;		/*!<The member that will be modified to simulate time */
	int pillAmount;							/*!<A memeber used in checking if the block is allowed to render */

	GameObject avatar;						/*!<A memeber to hold (or point) the Avatar, to allow access to the Avatar components*/
	//bool renderAllow;	
	float radius = 20;						/*!<Member that holds the distance that the block needs to be to the Avatar to render when the input <B>ShowPills</B> occurs*/
	public int amountOfPillsLost = 8;

	bool letRender;							/*!< A bool member with the highest priority in letting the block render, selected when <B>PreventShowingBlocks</B> input is released.*/

	/// 
	/// The <TT>Start()</TT> function sets the value of the decremeting timer (float decTimer, with float constTimer), as well as the decay timer ( float decrementDecayTimer with float decayTimer)
	///	and prevents all the cubes from rendering.
	/// 

	void Start() {
		
		//decTimer = constTimer;	
		this.gameObject.renderer.enabled = false;
		decrementDecayTimer = decayTimer;
	}
	
	 /*!  
     The <TT>Update()</TT> for this function is responsible for: 
     <OL>
     <LI>"Aging" the block once rendered.</LI>
     <LI>Rendereing all the blocks in a certain radius. </LI>
     <LI>Preventing the clocks from rendering if the player presses the appropriate button.</LI>	
     </OL>	 
     */				 
   
	void Update () {
		//code to age the block
		if ( rendered == true) {
			decrementDecayTimer -= Time.deltaTime;
			this.renderer.material.color = Color.Lerp ( this.renderer.material.color, Color.black, Time.deltaTime*3.8f/decayTimer);
			//this.renderer.material.Lerp ( this.renderer.material, secondMaterial, Time.deltaTime*3.8f/decayTimer);
			if ( decrementDecayTimer <= 0) {
				Destroy( this.gameObject );
			}
		}

		//to code to use the pills to show the blocks around the player
		if ( Input.GetButtonDown( "ShowBlocks") ) {
			avatar = GameObject.FindGameObjectWithTag("Player");
			bool renderAllow = ( avatar.GetComponent<CollectPills>().amountOfPills > 0 );
			float difference = Mathf.Abs( avatar.transform.position.sqrMagnitude - this.gameObject.transform.position.sqrMagnitude );
			if ( renderAllow && difference < radius ) {
				this.gameObject.renderer.enabled = true;
				//Debug.Log( radius + " " + avatar.transform.position.sqrMagnitude + " " + this.gameObject.transform.position.sqrMagnitude + " " + difference);
				//the player also loses pills when this event is called,
				//the losing of the pills is done in the CollectPills.cs script -- which the player has
			}

		}

		//code to prevent the cubes rendering on a specific button press
		if ( Input.GetButton("PreventShowingBlocks") ) {
			letRender = false;
		} else {
			letRender = true;
		}

		//****constant ease of access check to if rendered//
		if ( this.gameObject.renderer.enabled == true )
			rendered = true;
		else 
			rendered = false;

	}

	///
	/// 
	/// <TT>OnCollisionEnter()</TT> occurs on the collision between the block itself (this block) and any other collider. The player collider is explicitly
	/// checked for, and then factoring the amount of pills the player has, whether the block has been visited and if the player wants to render this cube
	/// the cube will either render or not, and in the case of the cube rendering, the amount of pills decreases by 1.
	/// 
	///

	
	void OnCollisionEnter(Collision c) {
		pillAmount = GameObject.FindGameObjectWithTag("Player").GetComponent<CollectPills>().amountOfPills;
		bool visited = this.gameObject.renderer.enabled;
		collided = true;
		if ( c.gameObject.tag == "Player" && pillAmount > 0 &&  !visited && letRender) {
				this.gameObject.renderer.enabled = true;
				GameObject.FindGameObjectWithTag("Player").GetComponent<CollectPills>().amountOfPills--;
				visited = true;	
		}
	}
}

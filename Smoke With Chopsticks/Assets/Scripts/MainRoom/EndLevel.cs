using UnityEngine;
using System.Collections;

///This script controls the conditons and the nature of the player reaching the destination.

public class EndLevel : MonoBehaviour {

	GameObject finish;		/*!<The Game Object representing the final destination.*/
	bool allowGUI;			/*!<A member that allows for caling of the GUI.*/
	float difference;		/*!<The win condition, as in the player needs to be certain distance away from the final destination.*/
	bool onGround;			/*!<The member that checks if the player in on the map*/
	ConclusionMenu cm;		/*!<A member that points to the <TT>ConclusionMenu.cs</TT> script in the object*/

	///
	///Accesses the final destination game object.
	///
	void Start () {
		finish = GameObject.FindGameObjectWithTag("Finish");
	}


	///
	///The <TT>Update()</TT> function constantly checks the win condition and sets allowGUI when the win condion becomes true.
	///
	void Update () {
		difference = Mathf.Abs( this.transform.position.sqrMagnitude - finish.transform.position.sqrMagnitude);
		onGround = Physics.Raycast( this.transform.position, Vector3.down);
		if (  difference < 800 && onGround) {
			allowGUI = true;
		} 
		//modular code in case the check is required again
		if (allowGUI) {
			cm = this.GetComponent<ConclusionMenu>();
			cm.allowGUI = true;
			cm.restart = false;
			cm.mainMenu = true;
			cm.exit = true;
			cm.message = "YOUR CURIOSITY SERVED YOU WELL, \nYOU FOUND WHAT YOU SOUGHT, \nTHAT WHICH YOU THOUGHT YOU DIDN'T KNOW BEFORE";
		}
	}
/*
	void OnGUI () {
		if (allowGUI) {
			Time.timeScale = 0;
			GUILayout.BeginArea( new Rect( 0, 0, Screen.width, Screen.height ) );
			GUILayout.BeginVertical();
			GUILayout.Box("YOUR CURIOSITY SERVED YOU WELL, \nYOU FOUND WHAT YOU SOUGHT, \nTHAT WHICH YOU THOUGHT YOU DIDN'T KNOW BEFORE");

			if ( GUILayout.Button("MAIN MENU") ) {
				Application.LoadLevel(0);
			}

			if ( GUILayout.Button("EXIT") ) {
				Application.Quit();
			}
			GUILayout.EndVertical();
			GUILayout.EndArea();
		}
	}
*/
}

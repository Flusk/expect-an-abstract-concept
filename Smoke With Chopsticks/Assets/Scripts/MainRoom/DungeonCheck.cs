using UnityEngine;
using System.Collections;

///The controller for creating the dungeon, works with the <TT>SpawnDungeon.cs</TT> script
public class DungeonCheck : MonoBehaviour {

	public int DungeonFloors;				/*!<A member to hold the number of tile sets on the map.*/
	Vector3 spawnPosition;					/*!<The position vector of the avatar's initial positon.*/
	public GameObject avatar;				/*!<A member pointing to the avatar.*/
	public GameObject finalDestination;		/*!<A member pointing to the object that represent the final destination.*/
		
	GameObject DunGen;						/*!<A member pointing to the initial Dungeon Spawner.*/
	GameObject [] DungeonSpawners;			/*!<An array holding all the Dungeon Spawners in the world.*/
	SpawnDungeon sd;						/*!<A memeber pointing to the <TT>SpawnDungeon.cs</TT> script.*/
	Vector3 finalPosition;					/*!<The postion vector of the final tile set.*/
	
	
	/*!
	The high level rule set for the dungeon generation:
	<OL>
	<LI>Access the <TT>SpawnDungeon.cs</TT> script.</LI>
	<LI>Continuously spanw tile sets until the amount of tile sets is equal to <VAR>DungeonFloors</VAR>. </LI>
	<LI>Hold the intial vector position.</LI>
	<LI>On the last tile set spawn, spawn the finalDestination object. </LI>
	<LI>Spawn the avatar above the first tile set.</LI>
	</OL>
	*/

	void Start () {
		DunGen = GameObject.FindGameObjectWithTag("DungeonGenerator");
		sd = DunGen.GetComponent<SpawnDungeon>();
		for ( int i = DungeonFloors; i > 0; --i	) {
			
			sd.BuildtoNextPosition();
			//on first build
			if ( i == DungeonFloors ) {
				spawnPosition = GameObject.FindGameObjectWithTag("Part").transform.position;
			}
			//on last build
			if ( i == 1) {
				finalPosition = DunGen.transform.position;
				Instantiate( finalDestination, finalPosition, Quaternion.identity);
			}
			
		}
		DungeonSpawners = GameObject.FindGameObjectsWithTag("DungeonGenerator");
		//Debug.Log ("Amount of DungeonSpawners: "+DungeonSpawners.Length);
		foreach ( GameObject go in DungeonSpawners) {
			Destroy(go);	
		}
		
		Instantiate( avatar, spawnPosition + Vector3.up*2f , Quaternion.identity);
	}
	
}

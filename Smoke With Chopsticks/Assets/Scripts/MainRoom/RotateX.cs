using UnityEngine;
using System.Collections;

///This script rotates the final destinantion, making it a little more evident.
public class RotateX : MonoBehaviour {

	///
	///A function to rotate in three axis.
	// Update is called once per frame
	void Update () {
		this.transform.Rotate (Vector3.one * 5, Space.World);
	}
}

using UnityEngine;
using System.Collections;

/*! \brief
The implementation of a random dungeon generation algorithm.
The algorithm works as follows:<BR>
<P>This script is attached to a component that is forced to traverse the world. Once a component of the dugeon has been created, or there is a need to create
another component of the dungeon, the object in the world space rotate around the y-axis and moves forward based on its own axis.<BR>
For randomness, everyone of these aspects is randomly selected.
*/


public class SpawnDungeon : MonoBehaviour {

	public GameObject [] dungeonParts;		/*!<An array of all the components in the dungeon*/
	public Vector3 initialPosition;			/*!<A position vector that holds the position of the first block created*/
	public GameObject thisIsACapsule;		/*!<A GameObject that the player wil try to collect which will be spawned throughout the dungeon*/
	public GameObject TC;
	
	int tileSelect;							/*!<A member which is used in randomly selecting the tile produced*/
	int direction;							/*!<The direction which the object is facing*/
	int seedCounter;						/*!<A member changes the seed appropriately*/
	int chanceClone;						/*!<A member that holds the possibilit of the object cloning itself*/

	int keepTrackCounter = 0;				/*!<A member to note the amount of clones there are*/

	//the y compoment of the direction vector
	int dir;								/*!<A member to hold the rotation around the y-axis*/

	//const float PHI_INVERSE = 1f/1.618f; //to enhance random geneation
	
	///Sets the seed counter
	void Start () {
		seedCounter = Random.Range (0, 100);	
	}
	
	///a public accessor for the very first block created
	public Vector3 GetInitialPosition() {
		return initialPosition;
	}
	
	///A way to ensure moderate randomness
	void Randomization (int seed) {
			//the following piece of code
			//is admittedly a hack
			Random.seed = (int) ( ( Random.Range ( (Random.value*( 0 - seed) ), ( Random.value*(++seed) ) ) )  );
			//Debug.Log( Random.seed);
	}
	
	///A function to produce random components of the dungeon
	GameObject selectTile () {
		Randomization( ++seedCounter );
		int rHold = (int) ( Random.Range(0, dungeonParts.Length) );
		if ( rHold == tileSelect) {
			return selectTile();	
		} else {
			tileSelect = rHold;	
		}
		return dungeonParts[tileSelect];	
	}
	
	///An abreviated function for creating a tile
	void CreateTile () {
		Instantiate( selectTile(), this.transform.position, Quaternion.identity );
	}
	
	///Randomly selects a direction our of four options
	int selectDirection() {
		Randomization( ++seedCounter );
		//the following piece of code
		//is admittedly a hack
		direction = (int) ( Random.Range(0, 3)  );	
		switch(direction) {
			case 0:
				return 0;
			case 1:
				return 90;
			case 2:
				return 180;
			case 3:
				return 270;
			default:
				return selectDirection();
		}
	}
	
	/*
	GameObject getDeadEnd() {
		for (int i = dungeonParts.Length; i <= 0; --i) {
			if ( dungeonParts[i].name == "DeadEnd")
				return dungeonParts[i];	
		}
			return dungeonParts[ dungeonParts.Length - 1 ];
	}
	
	GameObject getTrigger() {
		for (int i = dungeonParts.Length; i <= 0; --i) {
			if ( dungeonParts[i].name == "TriggerCube")
				return dungeonParts[i];	
		}
			return dungeonParts[ dungeonParts.Length - 1 ];
			
	}
	*/

	/*! \brief
	This function generates a single tile out of the set and behaves as follows:
	<OL>
	<LI>Set a (pseudo) random rotation.</LI>
	<LI>Move forward.</LI>
	<LI>Create a tile.</LI>
	<LI>Possible chance of making a clone.</LI>
	<LI>Possible chance of making a capsule (pill).</LI>
	</OL>
	*/

	public void BuildtoNextPosition() {
		Randomization( ++seedCounter );
		keepTrackCounter++;
		seedCounter++;
		//get a randomised direction
		dir  =  selectDirection();
		Vector3 rot = new Vector3 ( 0, dir, 0);
		this.transform.eulerAngles = rot;
		//Debug.Log(dir);
		this.transform.Translate( Vector3.forward, Space.Self );
		//Debug.Log(this.transform.position);
		CreateTile ();
		
		//fifty percent chance of cloning
		chanceClone =  (Mathf.FloorToInt ( Random.Range(0, 2) ) );
		if (chanceClone == 0) {
			createSelf();
		}

		/*if (keepTrackCounter >= 20){
			if ((Random.Range(0, 10) > 7))
				createTrigger();
		} */

		if ( Random.Range(0, 10) > 7) {

			createCap();
		}
	}
	
	///Abbreviated function for cloning this object
	void createSelf() {
		Instantiate ( this, this.transform.position, Quaternion.identity);
	}
	
	//Abbreviates function for creating a capsule
	void createCap() {
		Instantiate ( thisIsACapsule, this.transform.position + Vector3.up, Quaternion.identity);	
	}
	
	/*
	void createTrigger () {
		Instantiate ( TC, this.transform.position + Vector3.up, Quaternion.identity);		
	}
	*/
	
}

using UnityEngine;
using System.Collections;

///A script to ensure the camera follows the avatar with out having to parent the camera to the parent
public class FollowPlayer : MonoBehaviour {

	GameObject avatar; 		/*!<The memeber that points to the avatar */
	
	///
	///The <TT>Update</TT> function constantly accesses the player object and sets the object's x and z position to mimic the avatar's.
	///
	void Update () {
		avatar = GameObject.FindGameObjectWithTag("Player");
		this.gameObject.transform.position = new Vector3( avatar.transform.position.x, this.transform.position.y, avatar.transform.position.z);
	}
}

using UnityEngine;
using System.Collections;

/// <TT>ConclusionMenu</TT> is called and modified by the Avatar object once the game is over.

public class ConclusionMenu : MonoBehaviour {

	public bool allowGUI;		/*!< The member to activate the GUI*/
	public string message; 		/*!<The member that holds the message displayed in the GUI*/

	public bool restart;		/*!<The member that allows a restart button*/
	public bool mainMenu;		/*!<The member that allows a main menu button*/
	public bool exit;			/*!<The member that allows an exit button*/

	/*!
	The <TT>OnGUI()</TT> function int this script produces a box, which holds a conclusion message, and up to three buttons, namely:
	<UL>
	<LI>RESTART</LI>
	<LI>MAIN MENU</LI>
	<LI>EXIT</LI>
	</UL>
	*/


	void OnGUI () {
		if ( allowGUI ) {
			Time.timeScale = 0;
			GUILayout.BeginArea( new Rect( 0, 0, Screen.width, Screen.height ) );
			GUILayout.BeginVertical();
			GUILayout.Box(message);
			if (restart) 
				if ( GUILayout.Button("RESTART") ) {
					Application.LoadLevel(Application.loadedLevel);
				}

			if (mainMenu)
				if ( GUILayout.Button("MAIN MENU") ) {
					Application.LoadLevel(0);
				}

			if (exit)
				if ( GUILayout.Button("EXIT") ) {
					Application.Quit();
				}
			GUILayout.EndVertical();
			GUILayout.EndArea();
		}
	
	}
}

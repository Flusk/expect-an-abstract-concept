using UnityEngine;
using System.Collections;

public class LittleForces : MonoBehaviour {

	public float push;
	
	// Update is called once per frame
	void Update () {
		this.gameObject.rigidbody.AddForce(RandomVector3() * push);
	}

	Vector3 RandomVector3 () {
		Random.seed = (int) Time.time;
		return new Vector3(Random.value, Random.value, Random.value);
	}
}

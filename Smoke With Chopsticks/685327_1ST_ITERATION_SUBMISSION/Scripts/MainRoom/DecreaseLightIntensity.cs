using UnityEngine;
using System.Collections;

public class DecreaseLightIntensity : MonoBehaviour {

	
	public float decreaseTimer = 5.0f;
	private float decTimer;
	public float decreaseAmount = 0.1f;
	
	private float lightValue;

	CollectPills cp;

	// Use this for initialization
	void Start () {
		decTimer = decreaseTimer;
	
	}
	
	// Update is called once per frame
	void Update () {

	cp = this.GetComponent<CollectPills>();

	if ( cp.amountOfPills == 0 && this.light.intensity < 0 ) {
			//Debug.Log ("Over Here: "+decTimer);

		if (Time.time + decTimer <= Time.time) {
			lightValue = this.light.intensity -decreaseAmount;
			this.light.intensity  = lightValue;
			decTimer = decreaseTimer;
			//Debug.Log(cp.amountOfPills+ " "+ this.light.intensity);

		} else
			decTimer -= Time.deltaTime;
	} else if (cp.amountOfPills > 0) {
		if (Time.time + decTimer == Time.time) {
			cp.amountOfPills--;
			decTimer = decreaseTimer;

		} else
			decTimer -= Time.deltaTime;
	}

	
	}
}

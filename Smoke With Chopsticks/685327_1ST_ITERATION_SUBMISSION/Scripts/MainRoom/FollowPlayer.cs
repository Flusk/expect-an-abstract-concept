using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

	GameObject avatar;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		avatar = GameObject.FindGameObjectWithTag("Player");
		this.gameObject.transform.position = new Vector3( avatar.transform.position.x, this.transform.position.y, avatar.transform.position.z);
	}
}

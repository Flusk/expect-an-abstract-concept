using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	public Vector3 pushForce;
	public Vector3 turnSpeed;
	
	// Update is called once per frame
	void Update () {
		
		if ( Input.GetButton("Forward") ) {
			this.gameObject.rigidbody.AddForce(pushForce);
		}
		
		if ( Input.GetButton("Back") ) {
			this.gameObject.rigidbody.AddForce(-pushForce);
		}
		
		if ( Input.GetButtonDown("Left") ) {
			this.gameObject.transform.Rotate( turnSpeed );
		}
		
		if ( Input.GetButtonDown("Right") ) {
			this.gameObject.transform.Rotate( -turnSpeed );
		}
	
		
	
	}
}

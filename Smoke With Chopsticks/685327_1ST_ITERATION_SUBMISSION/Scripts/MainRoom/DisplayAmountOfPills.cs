using UnityEngine;
using System.Collections;

public class DisplayAmountOfPills : MonoBehaviour {

	void OnGUI () {
		GUILayout.BeginArea( new Rect( 0, 0, Screen.width, Screen.height ) );
		GUILayout.BeginVertical();
		GUILayout.Box ("Abstract Capsules Left: "+this.GetComponent<CollectPills>().amountOfPills);
		GUILayout.EndVertical();
		GUILayout.EndArea();
	}
}

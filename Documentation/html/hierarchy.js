var hierarchy =
[
    [ "MonoBehaviour", null, [
      [ "AllowToRender", "de/def/class_allow_to_render.html", null ],
      [ "BlockBehaviour", "d5/d31/class_block_behaviour.html", null ],
      [ "CollectPills", "d0/d3b/class_collect_pills.html", null ],
      [ "ConclusionMenu", "d3/dfd/class_conclusion_menu.html", null ],
      [ "DisplayAmountOfPills", "d2/d40/class_display_amount_of_pills.html", null ],
      [ "DungeonCheck", "d8/d1f/class_dungeon_check.html", null ],
      [ "EndLevel", "db/ddd/class_end_level.html", null ],
      [ "FollowPlayer", "dc/d1d/class_follow_player.html", null ],
      [ "GUIMenu", "d2/d5f/class_g_u_i_menu.html", null ],
      [ "Interpolate", "da/dab/class_interpolate.html", null ],
      [ "MovementOfThePlayer", "dc/d39/class_movement_of_the_player.html", null ],
      [ "OnThePosssibilityThatThePlayerFellOff", "d1/dad/class_on_the_posssibility_that_the_player_fell_off.html", null ],
      [ "RotateX", "d1/d52/class_rotate_x.html", null ],
      [ "SameAsAvatar", "d0/d87/class_same_as_avatar.html", null ],
      [ "SpawnDungeon", "de/d32/class_spawn_dungeon.html", null ],
      [ "ZoomInAndOut", "de/db7/class_zoom_in_and_out.html", null ]
    ] ]
];
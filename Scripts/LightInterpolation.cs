using UnityEngine;
using System.Collections;

public class LightInterpolation : MonoBehaviour {

	float fullLight; /*!< the light before interpolation */
	float numberOfCubes; /*!< the amount of cubes in the world */
	float lightProgression; /*!< the step between each colour interpolation */
	Vector4 backColor; /*!< changing the background colour */

	bool started = true;

	// Use this for initialization
	void Start () {
		//fullLight = GameObject.FindGameObjectsWithTag("Cube").Length;
	}
	
	// Update is called once per frame
	void Update () {
		/*******************************************************/
		//the following piece of code should be on start but that's not working at the moment
		if ( started ) {
			fullLight = GameObject.FindGameObjectsWithTag("Cube").Length;
			started = false;
		}
		/******************************************************/
		numberOfCubes = (float) ( GameObject.FindGameObjectsWithTag("Cube").Length );
		lightProgression = 1f - (numberOfCubes/fullLight);
		backColor = new Vector4( lightProgression, 0, 0, 1f );	
		this.camera.backgroundColor = (Color) (backColor);
	}
}

using UnityEngine;
using System.Collections;
/*	algorithm gathered from: 
* 	wikipedia.com:
* 			http://en.wikipedia.org/wiki/HSL_and_HSV#From_HSV
*/
/* KEY TO REMEMBER PLACEMENT
	 * H = X
	 * S = Y
	 * V = Z	*/
public class HSLtoRGB : MonoBehaviour{
	
	public Vector4 HSL = Vector4.zero;
	public Vector4 RGB = Vector4.zero;
	public float min = 0f, chroma = 0f, hdash = 0f, x = 0f;
	
	// Update is called once per frame
	public void Convert () {
		chroma = ( 1f - Mathf.Abs(2f * HSL.z - 1f) ) * HSL.y;
		hdash = HSL.x * (0.1667f);
		x = chroma * ( 1f - Mathf.Abs( hdash%2f - 1f ) );
		
		if ( hdash < 1.0f ) {
			RGB.x = chroma;
			RGB.y = x;
		} else if ( hdash < 2.0f ) {
			RGB.x = x;
			RGB.y = chroma;
		} else if ( hdash < 3.0f ) {
			RGB.y = chroma;
			RGB.z = x;
		} else if ( hdash < 4.0f) {
			RGB.y = x;
			RGB.z = chroma;
		} else if ( hdash < 5.0f ) {
			RGB.x = x;
			RGB.z = chroma;
		} else if ( hdash <= 6.0f ) {
			RGB.x = chroma;
			RGB.z = x;
		}
		
		min = HSL.z - 0.5f*chroma;
		RGB.x += min;
		RGB.y += min;
		RGB.z += min;
		if (RGB.z > 1.0f ) { RGB.z = Random.value; }
		RGB.w = HSL.w;
	
	}

	public Vector4 SimpleRGBtoHSL( Vector4 c) {
		float hue;

		if ( c.x == 1f && c.y == 0 && c.z == 0)
			hue = 0f;
		else if ( c.x == 0f && c.y == 1f && c.z == 0)
			hue = 1/120.0f;
		else if ( c.x == 0f && c.y == 0.0f && c.z == 1) 
			hue = 1/250;
		else 
			hue = 0f;

		return new Vector4 (hue, 1.0f, 1.0f, 1.0f);
	}

	public Vector4 FindDiscord (Vector4 color) {
		Vector4 newColor = new Vector4 ( color.x + 1f/90f, color.y, color.z, color.w);
		return newColor;

	}

	public Vector4 FindComplementary (Vector4 color) {
		Vector4 newColor = new Vector4 ( color.x + 1f/180f, color.y, color.z, color.w);
		return newColor;
	}
				
}

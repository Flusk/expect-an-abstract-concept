using UnityEngine;
using System.Collections;

public class SameAsAvatar : MonoBehaviour {

	
	GameObject avatar;
	/// <summary>
	///	The Start function access the component CollectPills
	/// <summary>
	void Start () {
		avatar = GameObject.FindGameObjectWithTag("Player");
	}	
	
	// Update is called once per frame
	void Update () {
		this.renderer.material.color = avatar.renderer.material.color;
	
	}
}

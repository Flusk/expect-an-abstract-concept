using UnityEngine;
using System.Collections;

public class MovementOfThePlayer : MonoBehaviour {

	public float Speed = 0.5f;
	public float TurnSpeed = 90f;
	
	private float directionRation;
	public bool onGround = false;
	
	public float constFallTimer = 3.0f;
	float FallTimer = 0;
	
	
	// Use this for initialization
	void Start () {
		FallTimer = constFallTimer;
		//this.gameObject.rigidbody.useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
		
		if ( this.gameObject.rigidbody.velocity.y < -1) {
			onGround = false;
			Debug.Log( this.gameObject.rigidbody.velocity.y );
		} else {
			onGround = true;
		}
		
		if (onGround) {
			//move forward
			if ( Input.GetButtonDown ( "Forward" ) ) {
					this.transform.Translate ( Vector3.forward *  Speed );
					//Debug.Log ( " This is a button push");
			}
			
			if (Input.GetButtonDown( "Back" ) ) {
					this.transform.Translate ( Vector3.back * Speed );
			}
			
			if ( Input.GetButtonDown ("Up") ) {
					this.transform.Rotate ( Vector3.up * TurnSpeed * Input.GetAxisRaw("Up") );
			}
			
			if ( !onGround ) {
					FallTimer -= Time.deltaTime;
					if ( FallTimer <= 0 ) {
						this.gameObject.rigidbody.useGravity = true;
					}
			}
		}
	}		
	
}

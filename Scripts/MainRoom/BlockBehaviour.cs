using UnityEngine;
using System.Collections;

public class BlockBehaviour : MonoBehaviour {

	
	
	public float constTimer = 3.0f;
	public float decTimer;
	//GameObject [] TileArray;
	CollectPills cp;
	int ap;
	public int amountOfRendered;
	public bool collided = false;

	public Material secondMaterial;


	bool rendered;
	public float decayTimer = 20.0f;
	public float decrementDecayTimer;
	int pillAmount;

	GameObject avatar;
	//bool renderAllow;
	float radius = 20;
	public int amountOfPillsLost = 8;

	bool letRender;

	///
	/// <summary> 
	/// The start function sets the value of the decremeting timer (float decTimer, with float constTimer), as well as the decay timer ( float decrementDecayTimer with float decayTimer)
	///	and prevents all the cubes from rendering.
	/// </summary>
	///

	void Start() {
		
		decTimer = constTimer;	
		this.gameObject.renderer.enabled = false;
		//TileArray = GameObject.FindGameObjectsWithTag("Cube"); 
		decrementDecayTimer = decayTimer;
	}
	
	// Update is called once per frame
	 /// <summary> 
        ///The update for this function is responsible for: (1) aging the block once rendered, (2) Rendereing the blocks in a certain radius, 
        ///	(3) Preventing the clocks from rendering if the player presses the appropriate button.			 
        ///				
        /// </summary> 
   
	void Update () {
		//code to age the block
		if ( rendered == true) {
			decrementDecayTimer -= Time.deltaTime;
			this.renderer.material.color = Color.Lerp ( this.renderer.material.color, Color.black, Time.deltaTime*3.8f/decayTimer);
			//this.renderer.material.Lerp ( this.renderer.material, secondMaterial, Time.deltaTime*3.8f/decayTimer);
			if ( decrementDecayTimer <= 0) {
				Destroy( this.gameObject );
			}
		}

		//to code to use the pills to show the blocks around the player
		if ( Input.GetButtonDown( "ShowBlocks") ) {
			avatar = GameObject.FindGameObjectWithTag("Player");
			bool renderAllow = ( avatar.GetComponent<CollectPills>().amountOfPills > 0 );
			float difference = Mathf.Abs( avatar.transform.position.sqrMagnitude - this.gameObject.transform.position.sqrMagnitude );
			if ( renderAllow && difference < radius ) {
				this.gameObject.renderer.enabled = true;
				//Debug.Log( radius + " " + avatar.transform.position.sqrMagnitude + " " + this.gameObject.transform.position.sqrMagnitude + " " + difference);
				//the player also loses pills when this event is called,
				//the losing of the pills is done in the CollectPills.cs script -- which the player has
			}

		}

		//code to prevent the cubes rendering on a specific button press
		if ( Input.GetButton("PreventShowingBlocks") ) {
			letRender = false;
		} else {
			letRender = true;
		}

		//****constant ease of access check to if rendered//
		if ( this.gameObject.renderer.enabled == true )
			rendered = true;
		else 
			rendered = false;

	}

	///
	/// <summary> 
	/// OnCollisionEnter occurs on the collision between the block itself (this block) and any other collider. The player collider is explicityl
	/// checked for, and then factoring the amount of pills the player has, whether the block has been visited and if the player wants to render this cube
	/// the cube will either render or not, and in the case of the cube rendering, the amount of pills decreases by 1
	/// </summary>
	///

	
	void OnCollisionEnter(Collision c) {
		pillAmount = GameObject.FindGameObjectWithTag("Player").GetComponent<CollectPills>().amountOfPills;
		bool visited = this.gameObject.renderer.enabled;
		collided = true;
		if ( c.gameObject.tag == "Player" && pillAmount > 0 &&  !visited && letRender) {
				this.gameObject.renderer.enabled = true;
				GameObject.FindGameObjectWithTag("Player").GetComponent<CollectPills>().amountOfPills--;
				visited = true;	
		}
	}
}

using UnityEngine;
using System.Collections;

public class ConclusionMenu : MonoBehaviour {

	public bool allowGUI;
	public string message;

	public bool restart, mainMenu, exit;

	void OnGUI () {
		if ( allowGUI ) {
			Time.timeScale = 0;
			GUILayout.BeginArea( new Rect( 0, 0, Screen.width, Screen.height ) );
			GUILayout.BeginVertical();
			GUILayout.Box(message);
			if (restart) 
				if ( GUILayout.Button("RESTART") ) {
					Application.LoadLevel(Application.loadedLevel);
				}

			if (mainMenu)
				if ( GUILayout.Button("MAIN MENU") ) {
					Application.LoadLevel(0);
				}

			if (exit)
				if ( GUILayout.Button("EXIT") ) {
					Application.Quit();
				}
			GUILayout.EndVertical();
			GUILayout.EndArea();
		}
	
	}
}

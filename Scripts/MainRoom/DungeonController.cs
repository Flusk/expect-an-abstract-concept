using UnityEngine;
using System.Collections;

public class DungeonController : MonoBehaviour {

	public int DungeonFloors;
	Vector3 spawnPosition;
	public GameObject avatar;
	public GameObject finalDestination;
	
	GameObject DunGen;
	GameObject [] DungeonSpawners;
	GameObject [] parts;
	SpawnDungeon sd;
	Vector3 finalPosition;
	
	
	void Start () {
		DunGen = GameObject.FindGameObjectWithTag("DungeonGenerator");
		sd = DunGen.GetComponent<SpawnDungeon>();
		for ( int i = DungeonFloors; i > 0; --i	) {
			DungeonSpawners = GameObject.FindGameObjectsWithTag("DungeonGenerator");
			foreach (GameObject dg in DungeonSpawners) {
				sd = dg.GetComponent<SpawnDungeon>();
			//if ( parts.Length >= DungeonFloors) {
				sd.BuildtoNextPosition();
			}
			//on first build
				if ( i == DungeonFloors ) {
					spawnPosition = GameObject.FindGameObjectWithTag("Part").transform.position;
				}
				//on last build
				if ( i == 1) {
					finalPosition = DunGen.transform.position;
					Instantiate( finalDestination, finalPosition, Quaternion.identity );
				}
		}
		DungeonSpawners = GameObject.FindGameObjectsWithTag("DungeonGenerator");
		Debug.Log ("Amount of DungeonSpawners: "+DungeonSpawners.Length);
		foreach ( GameObject go in DungeonSpawners) {
			Destroy(go);	
		}
		
		Instantiate( avatar, spawnPosition + Vector3.up*2f , Quaternion.identity);
	}
	
}

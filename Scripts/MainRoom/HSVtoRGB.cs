using UnityEngine;
using System.Collections;
/* 		algorithm gathered from:
 * 		beyondunreal wiki -- whom gathered it from the mathematical method on wikipedia
 * 			author:
 * 			date:	
 * 			url:	http://wiki.beyondunreal.com/HSV-RGB_Conversion
 * 			access:
 */
/* KEY TO REMEMBER PLACEMENT
	 * H = X
	 * S = Y
	 * V = Z	*/

public class HSVtoRGB : MonoBehaviour {
	
	
	//[RangeAttribute (0, 255) ]
	public Vector4 HSV = Vector4.zero;
	public Vector4 RGB = Vector4.zero;
	public float min = 0, chroma = 0, hdash = 0, x = 0;
	
	// Update is called once per frame
	public void Update() {
		Mathf.Clamp( RGB.z, 0, 1);
	}

	public void Convert () {
		chroma = HSV.y * HSV.z;
		hdash = HSV.x * (360/60);
		x = chroma *( 1 - Mathf.Abs( hdash%2 -1) );
		
		
		//shitload of checks for the conditions of HSV and then translated to RGB
		//i.e a split mathematical funtion
		if ( hdash < 1.0f ) {
			RGB.x = chroma;
			RGB.y = x;
		} else if ( hdash < 2.0f ) {
			RGB.x = x;
			RGB.y = chroma;
		} else if ( hdash < 3.0f ) {
			RGB.y = chroma;
			RGB.z = x;
		} else if ( hdash < 4.0f) {
			RGB.y = x;
			RGB.z = chroma;
		} else if ( hdash < 5.0f ) {
			RGB.x = x;
			RGB.z = chroma;
		} else if ( hdash <= 6.0f ) {
			RGB.x = chroma;
			RGB.z = x;
		}
		
		min = HSV.z - chroma;
		RGB.x += min;
		RGB.y += min;
		RGB.z += min;
		RGB.w = HSV.w;
		
		
	}
}

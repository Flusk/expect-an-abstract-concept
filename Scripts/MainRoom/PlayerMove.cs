using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {

	public float moveSpeed = 10.0f;
	public float turnSpeed = 10f;
	public float maxTurn = 90f;
	public float masss = 0.5f;
	public float jumpSpeed = 30.0f;
		
	private CharacterController myController;
	private Vector3 moveDirection;
//	private bool shallTurn = false;
	
	
	void Start() {
		myController = this.GetComponent<CharacterController>();	
	}
	
	// Update is called once per frame
	void Update () {
		
		//forwards backwards sideways
		//moveDirection.z =  Input.GetAxis("Vertical") * -moveSpeed;
		//moveDirection.z = Input.GetAxis("Forward") * moveSpeed;
		
		if ( Input.GetButton ("Forward") ) {
			this.transform.Translate( Vector3.forward * moveSpeed * Time.deltaTime * Input.GetAxisRaw("Forward") );
		}
		
		//rotate around the y-axis
		if ( Input.GetButtonDown("Up") ) {
				transform.Rotate( Vector3.up * maxTurn * Input.GetAxisRaw("Up"), Space.Self );
		}
		
		
			//if (maxTurn - turnSpeed > 0 )
			//transform.Rotate( Vector3.up * turnSpeed * Input.GetAxisRaw("Up"), Space.Self );
		//transform.Rotate( Vector3.up * -turnSpeed * Input.GetAxis("Right"), Space.Self );
		
		
		//jump
		if ( myController.isGrounded && Input.GetButton( "Jump" ) ) {
			moveDirection.y += 	jumpSpeed*Input.GetAxisRaw( "Jump" );
		}
			
		
		//set to relative
		moveDirection = transform.TransformDirection(moveDirection);
		
		//movement	
		myController.Move( moveDirection * Time.deltaTime );
		
		//gravity
		if (!myController.isGrounded) {
			moveDirection.y -= masss;
		}

	}
}

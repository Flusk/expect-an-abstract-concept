using UnityEngine;
using System.Collections;

public class ZoomInAndOut : MonoBehaviour {
	// Update is called once per frame
	public float divider = 0.1f;
	public float min = 4.0f;
	public float max = 30.0f;

	float yhold;
	void Update () {
		this.transform.Translate( Vector3.down * Input.GetAxis("Zoom") * Time.deltaTime * divider, Space.World);
		yhold = Mathf.Clamp ( this.transform.position.y, min, max );
		this.transform.position = new Vector3(this.transform.position.x, yhold, this.transform.position.z);
	}
}

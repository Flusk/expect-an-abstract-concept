using UnityEngine;
using System.Collections;

public class EndLevel : MonoBehaviour {

	GameObject finish;
	bool allowGUI;
	float difference;
	bool onGround;
	void Start () {
		finish = GameObject.FindGameObjectWithTag("Finish");
	}


	void Update () {
		difference = Mathf.Abs( this.transform.position.sqrMagnitude - finish.transform.position.sqrMagnitude);
		onGround = Physics.Raycast( this.transform.position, Vector3.down);
		if (  difference < 800 && onGround) {
			allowGUI = true;
		} 
	}

	void OnGUI () {
		if (allowGUI) {
			Time.timeScale = 0;
			GUILayout.BeginArea( new Rect( 0, 0, Screen.width, Screen.height ) );
			GUILayout.BeginVertical();
			GUILayout.Box("YOUR CURIOSITY SERVED YOU WELL, \nYOU FOUND WHAT YOU SOUGHT, \nTHAT WHICH YOU THOUGHT YOU DIDN'T KNOW BEFORE");

			if ( GUILayout.Button("MAIN MENU") ) {
				Application.LoadLevel(0);
			}

			if ( GUILayout.Button("EXIT") ) {
				Application.Quit();
			}
			GUILayout.EndVertical();
			GUILayout.EndArea();
		}
	}
}

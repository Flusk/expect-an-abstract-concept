using UnityEngine;
using System.Collections;

public class OnThePosssibilityThatThePlayerFellOff : MonoBehaviour {

	public float delayTime = 5.0f;
	float decTimer;
	GameObject [] blockArray;
	bool onGround = false;
	int fallTimes = 5;
	GameObject [] allGameObjects;
	ConclusionMenu cm;
	void Start () {
		decTimer = delayTime;
	}
	
	/* THE FOLLOWING BLOCK OF CODE CONSTATNTLY 
	*	CHECKS IF THE PLAYER IS ON A CUBE
	*	WHEN THAT IS NO TRUE ANY MORE
	*	THERE IS A DELAY OF FIVE SECONDS,
	*	AT THE END OF WHICH, THE PLAYER SPAWNS ON TO ONE
	*	OF THE OTHER BLOCKS IN THE DUNGEON
	*/
	void Update () {
		onGround = Physics.Raycast( this.transform.position, Vector3.down);
		if ( !onGround ) {
			if ( decTimer <= 0) {
				decTimer = delayTime;
				blockArray = GameObject.FindGameObjectsWithTag("Cube");
				int selectBlock = Random.Range(0, blockArray.Length);
				this.transform.position = blockArray[selectBlock].transform.position + Vector3.up;
				this.GetComponent<CollectPills>().amountOfPills = 0;
				fallTimes--;
			} else 
				decTimer -= Time.deltaTime;
		}

		if ( fallTimes <= 0) {
			cm = this.GetComponent<ConclusionMenu>();
			cm.allowGUI = true;
			cm.restart = false;
			cm.mainMenu = true;
			cm.exit = true;
			cm.message = "IT SEEMS YOU MIGHT HAVE FAILED,\nTHAT'S A PITY FOR WHAT YOU SEEK\nIT IS SOMETHING YOU MIGHT NEVER REACH.";
		}
	
	}
/*
	void OnGUI () {
		if ( fallTimes <= 0) {
			Time.timeScale = 0;
			//pausing all the gameobjects int the game
				allGameObjects = FindObjectsOfType (typeof(GameObject));
				foreach (GameObject g in allGameObjects) {
					g.SendMessage ("onPauseGame", SendMessage)
				}



			GUILayout.BeginArea( new Rect( 0, 0, Screen.width, Screen.height ) );
			GUILayout.BeginVertical();
			GUILayout.Box("IT SEEMS YOU MIGHT HAVE FAILED,\nTHAT'S A PITY FOR WHAT YOU SEEK\nIT IS SOMETHING YOU MIGHT NEVER REACH.");
			if ( GUILayout.Button("RESTART") ) {
				Application.LoadLevel(Application.loadedLevel);
			}

			if ( GUILayout.Button("MAIN MENU") ) {
				Application.LoadLevel(0);
			}

			if ( GUILayout.Button("EXIT") ) {
				Application.Quit();
			}
			GUILayout.EndVertical();
			GUILayout.EndArea();
		}
	}
	*/
}

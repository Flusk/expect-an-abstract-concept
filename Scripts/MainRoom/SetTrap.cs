using UnityEngine;
using System.Collections;

public class SetTrap : MonoBehaviour {
	
	public float ColorChangeTime = 30.0f;
	
	bool hasCollided = false;
	float timer = 0;
	//Collider hold;
	
	void OnTriggerStay(Collider c) {
		if ( c.gameObject.tag == "Switch")
			hasCollided = true;

	}

	void OnTriggerEnter (Collider c) {
		if ( c.gameObject.tag == "Switch") 
			c.gameObject.audio.Play();
	}
	
	void Update()	{
		if (hasCollided)	{
			//hold.gameObject.audio.Play();
			timer += Time.deltaTime / ColorChangeTime;
			this.gameObject.camera.backgroundColor = Color.Lerp(Color.black, Color.white, timer);
			this.gameObject.light.color = Color.Lerp(Color.blue, Color.red, timer);
			if (timer > ColorChangeTime)	{
				timer = 0;
				hasCollided = false;
			}
		}
	}
}

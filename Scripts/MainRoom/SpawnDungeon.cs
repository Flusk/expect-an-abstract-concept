using UnityEngine;
using System.Collections;

public class SpawnDungeon : MonoBehaviour {

	public GameObject [] dungeonParts;
	public Vector3 initialPosition;
	public GameObject thisIsACapsule;
	public GameObject TC;
	
	int tileSelect;
	int direction;
	int seedCounter;
	int chanceClone;

	int keepTrackCounter = 0;

	//the y compoment of the direction vector
	int dir;

	const float PHI_INVERSE = 1f/1.618f; //to enhance random geneation
	
	/*public enum dungeonTiles {
			Straight,
			Corner,
			Tj,
			DeadEnd,
			FourWay
	};
	*/
	
	void Start () {
		seedCounter = Random.Range (0, 100);	
	}
	
	public Vector3 GetInitialPosition() {
		return initialPosition;
	}
	
	void Randomization (int seed) {
			//the following piece of code
			//is admittedly a hack
			Random.seed = (int) ( ( Random.Range ( (Random.value*( 0 - seed) ), ( Random.value*(++seed) ) ) )  );
			//Debug.Log( Random.seed);
	}
	
	GameObject selectTile () {
		Randomization( ++seedCounter );
		int rHold = (int) ( Random.Range(0, dungeonParts.Length) );
		if ( rHold == tileSelect) {
			return selectTile();	
		} else {
			tileSelect = rHold;	
		}
		return dungeonParts[tileSelect];	
	}
	
	void CreateTile () {
		Instantiate( selectTile(), this.transform.position, Quaternion.identity );
	}
	
	int selectDirection() {
		Randomization( ++seedCounter );
		//the following piece of code
		//is admittedly a hack
		direction = (int) ( Random.Range(0, 3)  );	
		switch(direction) {
			case 0:
				return 0;
			case 1:
				return 90;
			case 2:
				return 180;
			case 3:
				return 270;
			default:
				return selectDirection();
		}
	}
	
	GameObject getDeadEnd() {
		for (int i = dungeonParts.Length; i <= 0; --i) {
			if ( dungeonParts[i].name == "DeadEnd")
				return dungeonParts[i];	
		}
			return dungeonParts[ dungeonParts.Length - 1 ];
	}
	
	GameObject getTrigger() {
		for (int i = dungeonParts.Length; i <= 0; --i) {
			if ( dungeonParts[i].name == "TriggerCube")
				return dungeonParts[i];	
		}
			return dungeonParts[ dungeonParts.Length - 1 ];
			
	}
	
	public void BuildtoNextPosition() {
		Randomization( ++seedCounter );
		keepTrackCounter++;
		seedCounter++;
		//get a randomised direction
		dir  =  selectDirection();
		Vector3 rot = new Vector3 ( 0, dir, 0);
		this.transform.eulerAngles = rot;
		//Debug.Log(dir);
		this.transform.Translate( Vector3.forward, Space.Self );
		//Debug.Log(this.transform.position);
		CreateTile ();
		
		//fifty percent chance of cloning
		chanceClone =  (Mathf.FloorToInt ( Random.Range(0, 2) ) );
		if (chanceClone == 0) {
			createSelf();
		}

		/*if (keepTrackCounter >= 20){
			if ((Random.Range(0, 10) > 7))
				createTrigger();
		} */

		if ( Random.Range(0, 10) > 7) {

			createCap();
		}
	}
	
	void createSelf() {
		Instantiate ( this, this.transform.position, Quaternion.identity);
	}
	
	void createCap() {
		Instantiate ( thisIsACapsule, this.transform.position + Vector3.up, Quaternion.identity);	
	}
	
	void createTrigger () {
		Instantiate ( TC, this.transform.position + Vector3.up, Quaternion.identity);		
	}
	
}

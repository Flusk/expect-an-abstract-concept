using UnityEngine;
using System.Collections;

public class AllowToRender : MonoBehaviour {

	public bool allowRender = false; 	/*!< A boolean member that acts as switch to allow the blocks to render */
	public int amountOfPillsLost = 5;	/*!< The damage to the amount of pills taken from interacting with the block */
	
	
	
	void Update () {
		if ( Input.GetButtonDown( "ShowBlocks") ) {
			allowRender = true;
			this.GetComponent<CollectPills>().amountOfPills -= amountOfPillsLost;
			this.GetComponent<CollectPills>().amountOfPills = (int) Mathf.Clamp(this.GetComponent<CollectPills>().amountOfPills, 0, Mathf.Infinity);
		}
	
	}
}

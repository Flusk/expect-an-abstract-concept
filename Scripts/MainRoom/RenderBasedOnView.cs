using UnityEngine;
using System.Collections;

public class RenderBasedOnView : MonoBehaviour {

	
	public float constTimer = 3.0f;
	public float decTimer;
	//GameObject [] TileArray;
	CollectPills cp;
	int ap;
	public int amountOfRendered;
	public bool collided = false;
	
	void Start() {
		//this.gameObject.AddComponent<Renderer>();
		decTimer = constTimer;	
		this.gameObject.renderer.enabled = false;
		//cp = 
		//TileArray = GameObject.FindGameObjectsWithTag("Cube"); 
	}
	
	// Update is called once per frame
	void Update () {
		//ap = this.gameObject.GetComponent<CollectPills>().amountOfPills;
		if ( this.gameObject.renderer.isVisible ) {
			if ( decTimer <= 0) {
				decTimer = constTimer;
				gameObject.renderer.enabled = true;
			}
			else {
				decTimer--;
			}
			//this.gameObject.renderer.enabled = true;
		} 
		
	}
	
	void OnCollisionEnter(Collision c) {
		collided = true;
		if ( c.gameObject.tag == "Player" ) {
				this.gameObject.renderer.enabled = true;	
		}
	}
}

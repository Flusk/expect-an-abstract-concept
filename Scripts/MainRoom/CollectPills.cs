using UnityEngine;
using System.Collections;

public class CollectPills : MonoBehaviour {
	
	public int amountOfPills = 10;
	public int minPilsGotten = 5;
	public int maxPilsGotten = 10;
	

	void OnTriggerEnter (Collider c) {
		amountOfPills = Mathf.Clamp(amountOfPills, 0, 60);
		if ( c.gameObject.tag == "Collectible") {
			amountOfPills += Random.Range( minPilsGotten, maxPilsGotten);
			Destroy(c.gameObject);
		}
	}
}

using UnityEngine;
using System.Collections;

public class MovementOfThePlayer : MonoBehaviour {

	public float ForwardSpeed = 6f;
	public float TurnSpeed = 90f;
	
	private float directionRatio;
	
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		//move forward
		if ( Input.GetButton ( "Forward" ) ) {
				this.transform.Translate ( Vector3.forward * ForwardSpeed * Time.deltaTime );
				//Debug.Log ( " This is a button push");
		}
		
		if (Input.GetButton( "Back" ) ) {
				this.transform.Translate ( -Vector3.forward * ForwardSpeed * Time.deltaTime );
		}
		
		if ( Input.GetButtonDown ("Up") ) {
				this.transform.Rotate ( Vector3.up * TurnSpeed * Input.GetAxisRaw("Up") );
		}
		
	}
}

using UnityEngine;
using System.Collections;

public class CollectPills : MonoBehaviour {

	public int amountOfPills = 0;

	void OnTriggerEnter (Collider c) {
		if ( c.gameObject.tag == "Collectible") {
			amountOfPills++;
			Destroy(c.gameObject);
		}
	}
}

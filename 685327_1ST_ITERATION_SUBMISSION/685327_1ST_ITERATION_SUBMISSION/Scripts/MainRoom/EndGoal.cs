using UnityEngine;
using System.Collections;

public class EndGoal : MonoBehaviour {

	void onCollisionEnter (Collision c) {
		HSVtoRGB converter  = this.gameObject.GetComponent<HSVtoRGB>();
		if (c.gameObject.tag == "Player") {
			converter.RGB = randomSolidColour();
			converter.Convert();
			this.gameObject.renderer.material.color = converter.HSV;
		}	
	}
	
	//a vector3 with components between 0 and 1
	Vector4 randomSolidColour () {
		Random.seed = (int) Time.time;
		return new Vector4(Random.value, Random.value, Random.value, 1);
	}
}

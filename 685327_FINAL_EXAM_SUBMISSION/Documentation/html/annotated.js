var annotated =
[
    [ "AllowToRender", "de/def/class_allow_to_render.html", "de/def/class_allow_to_render" ],
    [ "BlockBehaviour", "d5/d31/class_block_behaviour.html", "d5/d31/class_block_behaviour" ],
    [ "CollectPills", "d0/d3b/class_collect_pills.html", "d0/d3b/class_collect_pills" ],
    [ "ConclusionMenu", "d3/dfd/class_conclusion_menu.html", "d3/dfd/class_conclusion_menu" ],
    [ "DisplayAmountOfPills", "d2/d40/class_display_amount_of_pills.html", "d2/d40/class_display_amount_of_pills" ],
    [ "DungeonCheck", "d8/d1f/class_dungeon_check.html", "d8/d1f/class_dungeon_check" ],
    [ "EndLevel", "db/ddd/class_end_level.html", "db/ddd/class_end_level" ],
    [ "FollowPlayer", "dc/d1d/class_follow_player.html", "dc/d1d/class_follow_player" ],
    [ "GUIMenu", "d2/d5f/class_g_u_i_menu.html", "d2/d5f/class_g_u_i_menu" ],
    [ "Interpolate", "da/dab/class_interpolate.html", "da/dab/class_interpolate" ],
    [ "MovementOfThePlayer", "dc/d39/class_movement_of_the_player.html", "dc/d39/class_movement_of_the_player" ],
    [ "OnThePosssibilityThatThePlayerFellOff", "d1/dad/class_on_the_posssibility_that_the_player_fell_off.html", "d1/dad/class_on_the_posssibility_that_the_player_fell_off" ],
    [ "RotateX", "d1/d52/class_rotate_x.html", "d1/d52/class_rotate_x" ],
    [ "SameAsAvatar", "d0/d87/class_same_as_avatar.html", "d0/d87/class_same_as_avatar" ],
    [ "SpawnDungeon", "de/d32/class_spawn_dungeon.html", "de/d32/class_spawn_dungeon" ],
    [ "ZoomInAndOut", "de/db7/class_zoom_in_and_out.html", "de/db7/class_zoom_in_and_out" ]
];
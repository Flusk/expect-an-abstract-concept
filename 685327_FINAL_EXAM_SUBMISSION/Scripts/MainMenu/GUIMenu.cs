using UnityEngine;
using System.Collections;

///This controls the main menu
public class GUIMenu : MonoBehaviour {
	/// This <TT>OnGUI</TT> function displays the main menu and provides two buttons:
	///<UL>
	///<LI>START</LI>
	///<LI>EXIT</LI>
	///</UL> 
	void OnGUI () {
		GUILayout.BeginArea( new Rect( 0, 0, Screen.width, Screen.height ) );
		GUILayout.BeginVertical();
		GUILayout.Box("FOLLOW YOUR CURIOSITY \nFOLLOW YOUR MIND\nFOLLOW THE LIGHT \n\nWASD TO MOVE	\n\nLEFT CLICK TO SEE CUBES\n\nRIGHT CLICK TO KEEP CUBES IN DARKNESS\n\nSCROLL TO ZOOM");
		if ( GUILayout.Button("START") ) {
			Application.LoadLevel(1);
		}

		if ( GUILayout.Button("EXIT") ) {
			Application.Quit();
		}
		GUILayout.EndVertical();
		GUILayout.EndArea();

	}
}


	/*!
	*	\mainpage Some interesting infromation on Smokes With Chopsticks
	*
	*	\section intro_sec INTRODUCTION
	*	This is an introduction
	*	This is the documenaion for an abstract
	*	game developed in the Unity Engine, using the C# language.
	*	The game (titled Smoke With Chopsticks) focuses on the elements of dungeon
	*	crawling and survival.
	*
	*	\section background BACKGROUND
	*	The game was a project focusing utilising abstract concepts universally. This explicitly
	*	called for a focus on slightly lower level simplicity by focusing on mechanics for most of the time
	*	with aesthetic mostly being utilised to ensure communication and player manipulation. The code was encouraged to 
	*	be as simple as possible in line with the focus on abstract aesthetic and design.
	*
	*	\section source SOURCE
	*	A copy of the project source can be downloaded from  
	*	<a href="https://bitbucket.org/VQZX/expect-an-abstract-concept/get/c926c54ac4c8.zip"><B>this BitBucket repository.</B></a>
	*
	*	\section webbuild WEB BUILD
	*	The game can be found and played at 
	*	<a href="http://bit.ly/17m4kKZ" target="_blank"><B>this link.</B></a>
	*/

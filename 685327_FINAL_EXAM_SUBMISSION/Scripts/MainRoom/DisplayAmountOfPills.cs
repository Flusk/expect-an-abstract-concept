using UnityEngine;
using System.Collections;

///A method of communicating the amount of pills that player has by changing the color of the Avatar.
public class DisplayAmountOfPills : MonoBehaviour {

	public int pillAmount; /*!< A member to hold the amount of pills.*/
	int fullColorAmount = 60; //!< A member to compare against the amount of pills -- the avatar will not increase in colour passed this point. */
	CollectPills cp;	/*!< The component that holds the information. */
	Color avatarColor;	/*!< A member to hold the colour of the avatar */
	float ratio;
	
	///
	///	The <TT>Update()</TT> function sets the R (red) and B (blue) components of the <VAR>avatarColor</VAR> as inverses of each other
	/// based on the <VAR>amountOfPills</VAR> in the <TT>CollectPills.cs</TT> script.
	///
	void Update () {
		//get the appropriate values
		cp = this.gameObject.GetComponent<CollectPills>();
		pillAmount = cp.amountOfPills;
		ratio = (float) pillAmount/fullColorAmount;
		avatarColor.b = 1 - ratio;
		avatarColor.b = Mathf.Clamp01 ( avatarColor.b );
		avatarColor.r = ratio;
		avatarColor.r = Mathf.Clamp01 ( avatarColor.r );

		//set the colour of the avatar
		this.gameObject.renderer.material.color = avatarColor;
		//Debug.Log(avatarColor);
	}
}

using UnityEngine;
using System.Collections;

///<TT>CollectPills.cs</TT> is a script that allows the player to collect pills and processors the appropriate calculations.
public class CollectPills : MonoBehaviour {
	
	public int amountOfPills = 10;		/*!<The member that keeps track of the pils resource */
	public int minPilsGotten = 5;		/*!<The minimun amount of pills recieve from a collection */
	public int maxPilsGotten = 10;		/*!<The maximum amount of pills recieve from a collection */
	

	///
	/// <TT>OnTriggerEnter()</TT> notes a collision between the Avatar and the <VAR>capsule</VAR> or <VAR>pill</VAR> object (noted as a "Collectible") and modifes the <VAR>amountOfPills</VAR> member.
	///

	void OnTriggerEnter (Collider c) {
		amountOfPills = Mathf.Clamp(amountOfPills, 0, 60);
		if ( c.gameObject.tag == "Collectible") {
			amountOfPills += Random.Range( minPilsGotten, maxPilsGotten);
			Destroy(c.gameObject);
		}
	}
}

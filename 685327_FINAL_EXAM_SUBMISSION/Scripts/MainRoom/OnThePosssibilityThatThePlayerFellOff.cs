using UnityEngine;
using System.Collections;

///This script is controls the conditons and events of the lose conditon

public class OnThePosssibilityThatThePlayerFellOff : MonoBehaviour {

	public float delayTime = 5.0f;			/*!<The member that hold the time before the avatar is reset to  one of the positions on the map.*/
	float decTimer;							/*!<The member that is modified to simulate time.*/
	GameObject [] blockArray;				/*!<An array of all the blocks on the map.*/
	bool onGround = false;					/*!<A member to check if the avatar is on the ground.*/
	int fallTimes = 5;						/*!<The amount of times the avatar can fall before losing the game.*/
	//GameObject [] allGameObjects;			
	ConclusionMenu cm;						/*!<A member to point ot <TT>ConclusionMenu.cs</TT> script.*/
	
	///
	///The <TT>Start()</TT> funcion sets <VAR>decTime</VAR> to <VAR>delayTime<VAR>
	void Start () {
		decTimer = delayTime;
		Random.seed = (int) Time.time;
	}
	
	///The <TT>Update()</TT> function controls two related aspects of the when the player falls:
	///<OL>
	///<LI>Repositioning the player to a random position on the map.</LI>
	///<LI>Ending the game after the player falls three (3) times.</LI>
	///
	void Update () {
		onGround = Physics.Raycast( this.transform.position, Vector3.down);
		if ( !onGround ) {
			if ( decTimer <= 0) {
				decTimer = delayTime;
				blockArray = GameObject.FindGameObjectsWithTag("Cube");
				int selectBlock = Random.Range(0, blockArray.Length);
				this.transform.position = blockArray[selectBlock].transform.position + Vector3.up;
				this.GetComponent<CollectPills>().amountOfPills = 0;
				fallTimes--;
			} else 
				decTimer -= Time.deltaTime;
		}

		if ( fallTimes <= 0) {
			cm = this.GetComponent<ConclusionMenu>();
			cm.allowGUI = true;
			cm.restart = false;
			cm.mainMenu = true;
			cm.exit = true;
			cm.message = "IT SEEMS YOU MIGHT HAVE FAILED,\nTHAT'S A PITY FOR WHAT YOU SEEK,\nIT IS SOMETHING YOU MIGHT NEVER REACH.";
		}
	
	}
/*
	void OnGUI () {
		if ( fallTimes <= 0) {
			Time.timeScale = 0;
			//pausing all the gameobjects int the game
				allGameObjects = FindObjectsOfType (typeof(GameObject));
				foreach (GameObject g in allGameObjects) {
					g.SendMessage ("onPauseGame", SendMessage)
				}



			GUILayout.BeginArea( new Rect( 0, 0, Screen.width, Screen.height ) );
			GUILayout.BeginVertical();
			GUILayout.Box("IT SEEMS YOU MIGHT HAVE FAILED,\nTHAT'S A PITY FOR WHAT YOU SEEK\nIT IS SOMETHING YOU MIGHT NEVER REACH.");
			if ( GUILayout.Button("RESTART") ) {
				Application.LoadLevel(Application.loadedLevel);
			}

			if ( GUILayout.Button("MAIN MENU") ) {
				Application.LoadLevel(0);
			}

			if ( GUILayout.Button("EXIT") ) {
				Application.Quit();
			}
			GUILayout.EndVertical();
			GUILayout.EndArea();
		}
	}
	*/
}

using UnityEngine;
using System.Collections;

///Allows the camera to zoom in and out (i.e. travel through the y-dimension).
public class ZoomInAndOut : MonoBehaviour {
	// Update is called once per frame
	public float divider = 0.1f;	/*!<A member to control the speed of the movement*/
	public float min = 4.0f;		/*!<The greates zoom in.*/
	public float max = 30.0f;		/*!<The greatest zoom out*/
	
	float yhold;					/*!<Holds the y-position for secutiy reasons, and is used in clamping*/
	void Update () {
		this.transform.Translate( Vector3.down * Input.GetAxis("Zoom") * Time.deltaTime * divider, Space.World);
		yhold = Mathf.Clamp ( this.transform.position.y, min, max );
		this.transform.position = new Vector3(this.transform.position.x, yhold, this.transform.position.z);
	}
}

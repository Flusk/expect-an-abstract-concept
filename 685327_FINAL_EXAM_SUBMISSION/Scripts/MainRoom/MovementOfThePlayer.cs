using UnityEngine;
using System.Collections;

///A script that knows what it is.
public class MovementOfThePlayer : MonoBehaviour {

	public float Speed = 0.5f;				/*!<The amount of displacement the object experience at on each forward movement*/
	public float TurnSpeed = 90f;			/*!<The amount of roation on eacturn*/
			
	public bool onGround = false;			/*!<A member to check if the avatar is on the map*/
	
	public float constFallTimer = 3.0f;
	float FallTimer = 0;
	
	
	// Use this for initialization
	void Start () {
		FallTimer = constFallTimer;
		//this.gameObject.rigidbody.useGravity = false;
	}
	
	///
	///The <TT>Update()</TT> function holds the movement rule set:
	///<OL>
	///<LI>Check in on the map.</LI>
	///<LI>On the the <B>Forward</B> input, move the object forward by the amount in <VAR>Speed</VAR>.</LI>
	///<LI>On the <B>Up</B> input, rotate the player based on the button press, by the amount in <VAR>TurnSpeed</VAR>.</LI>
	///</OL>
	void Update () {
		
		if ( this.gameObject.rigidbody.velocity.y < -1) {
			onGround = false;
		} else {
			onGround = true;
		}
		
		if (onGround) {
			//move forward
			if ( Input.GetButtonDown ( "Forward" ) ) {
					this.transform.Translate ( Vector3.forward *  Speed, Space.Self );
					//Debug.Log ( " This is a button push");
			}
			
			if (Input.GetButtonDown( "Back" ) ) {
					this.transform.Translate ( Vector3.back * Speed );
			}
			
			if ( Input.GetButtonDown ("Up") ) {
					this.transform.Rotate ( Vector3.up * TurnSpeed * Input.GetAxisRaw("Up") );
			}
			
			if ( !onGround ) {
					FallTimer -= Time.deltaTime;
					if ( FallTimer <= 0 ) {
						this.gameObject.rigidbody.useGravity = true;
					}
			}
		}
	}		
	
}

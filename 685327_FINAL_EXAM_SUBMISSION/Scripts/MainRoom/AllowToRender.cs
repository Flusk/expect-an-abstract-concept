using UnityEngine;
using System.Collections;

///AllowToRender.cs is a sibling script of a block of code in <TT>BlockBehaviour</TT>. Both check for the <B>ShowBlocks</B> input and react appropriately. <TT>AllowToRender.cs</TT> simply decrements the <VAR>amountOfPills</VAR> the avatar has by <VAR>amountOfPillsLost</VAR>.
public class AllowToRender : MonoBehaviour {

	public bool allowRender = false; 	/*!< A boolean member that acts as switch to allow the blocks to render */
	public int amountOfPillsLost = 5;	/*!< The damage to the amount of pills taken from interacting with the block */
	///
	/// This <TT>Update()</TT> function checks for the <B>ShowBlocks</B> input and:<BR>
	///	<OL>
	/// <LI>Sets a boolean value <VAR>allowRender</VAR> to true.</LI>
	/// <LI>Decrements the <VAR>amountOfPills</VAR> in the <TT>CollectPills</TT> Component.</LI>
	/// <LI>As well as clamp the <VAR>amountOfPills</VAR> variable to a specific amount.

	
	void Update () {
		if ( Input.GetButtonDown( "ShowBlocks") ) {
			allowRender = true;
			this.GetComponent<CollectPills>().amountOfPills -= amountOfPillsLost;
			this.GetComponent<CollectPills>().amountOfPills = (int) Mathf.Clamp(this.GetComponent<CollectPills>().amountOfPills, 0, 60);
		}
	
	}
}

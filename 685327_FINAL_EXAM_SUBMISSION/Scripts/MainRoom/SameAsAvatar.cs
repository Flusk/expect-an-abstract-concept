using UnityEngine;
using System.Collections;

///A script to ensure this object (which is attached to the avatar) is the same color.
public class SameAsAvatar : MonoBehaviour {

	
	GameObject avatar;	/*!<A member to point to the avatar. */
	/// 
	///	The <TT>Start()</TT> function access the component CollectPills.
	///
	void Start () {
		avatar = GameObject.FindGameObjectWithTag("Player");
	}	
	
	// Update is called once per frame
	/// 
	///	The <TT>Update</TT> functions ensure that this object is always the same colour as the player.
	/// 
	void Update () {
		this.renderer.material.color = avatar.renderer.material.color;
	
	}
}
